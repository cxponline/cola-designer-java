package com.cola.colaboot.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.system.mapper.SysDictMapper;
import com.cola.colaboot.module.system.service.SysDictService;
import com.cola.colaboot.module.system.pojo.SysDict;
import org.springframework.stereotype.Service;

@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {
    @Override
    public IPage<SysDict> pageList(SysDict sysDict, Integer pageNo, Integer pageSize) {
        LambdaQueryWrapper<SysDict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(sysDict.getDictGroup()),SysDict::getDictGroup,sysDict.getDictGroup());
        queryWrapper.eq(StringUtils.isNotBlank(sysDict.getDictTag()),SysDict::getDictTag,sysDict.getDictTag());
        queryWrapper.orderByDesc(SysDict::getCreateTime);
        Page<SysDict> page = new Page<>(pageNo, pageSize);
        return page(page, queryWrapper);
    }

    @Override
    public SysDict getByTag(String tag) {
        LambdaQueryWrapper<SysDict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysDict::getDictTag,tag);
        queryWrapper.last("limit 1");
        return getOne(queryWrapper);
    }
}
