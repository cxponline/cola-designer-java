package com.cola.colaboot.module.design.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cola.colaboot.module.design.pojo.DesignImgPool;
import org.apache.ibatis.annotations.Param;

public interface DesignImgPoolMapper extends BaseMapper<DesignImgPool> {
    IPage<DesignImgPool> pageList(Page<DesignImgPool> page, @Param("pool") DesignImgPool pool);
}
