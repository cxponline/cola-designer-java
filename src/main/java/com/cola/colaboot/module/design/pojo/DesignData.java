package com.cola.colaboot.module.design.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@TableName("design_data")
@Data
public class DesignData {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String title;
    private String simpleDesc;
    private String bgImg;
    private String bgColor;
    private Integer scaleX;
    private Integer scaleY;
    private String components;
    private String designImgPath;//保存后生成的设计图
    private Integer state;//禁用状态：1启用,2禁用,-1删除
    private String viewCode;
    private String countView;
    private String createUser;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
