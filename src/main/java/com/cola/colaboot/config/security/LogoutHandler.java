package com.cola.colaboot.config.security;

import com.alibaba.fastjson.JSONObject;
import com.cola.colaboot.config.dto.Res;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class LogoutHandler implements LogoutSuccessHandler {
    /**
     *  退出登录删掉redis中的token
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        PrintWriter writer = response.getWriter();
        writer.write(JSONObject.toJSONString(Res.ok("退出登录成功")));
        writer.close();
    }
}
