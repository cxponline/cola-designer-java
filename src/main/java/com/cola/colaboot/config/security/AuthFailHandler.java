package com.cola.colaboot.config.security;

import com.alibaba.fastjson.JSONObject;
import com.cola.colaboot.config.dto.Res;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class AuthFailHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse response, AuthenticationException e) throws IOException {
        Res<?> fail = Res.fail("登录状态失效，请重新登录！");
        fail.setCode(302);
        response.setStatus(200);
        PrintWriter writer = response.getWriter();
        writer.write(JSONObject.toJSONString(fail));
        writer.close();
    }
}
